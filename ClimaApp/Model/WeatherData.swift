//
//  WeatherData.swift
//  ClimaApp
//
//  Created by HPM on 01/08/2019.
//  Copyright © 2019 HPM. All rights reserved.
//

import Foundation

class WeatherData {
    
    var temp: Double
    var humidity: Double
    var max: Double
    var min: Double
    var windSpeed: Double
    var clouds: Double
    var location: String
    var code: String
    var id: Int
    
    init(temp: Double, humidity: Double, max: Double, min: Double, windSpeed: Double, clouds: Double, location: String, code: String, id: Int) {
        
        self.temp = floor(1000*(temp-Double(273.15))/1000)
        self.humidity = floor(1000*humidity/1000)
        self.max = floor(1000*(max-Double(273.15))/1000)
        self.min = floor(1000*(min-Double(273.15))/1000)
        self.windSpeed = windSpeed
        self.clouds = clouds
        self.location = location
        self.code = code
        self.id = id
    }
    
    func determineType() -> String {
        
        var type : String = ""
        
        let iconCode = String(id)
        if iconCode == "800" {
            type = "Clear"
        }
        else if iconCode.prefix(2) == "80" {
            type = "Clouds"
        }
        else {
            switch iconCode.prefix(1) {
            case "2" :
                type = "Thunderstorm"
                break;
            case "3" :
                type = "Drizzle"
                break;
            case "5" :
                type = "Rain"
                break;
            case "6" :
                type = "Snow"
                break;
            case "7" :
                type = "Atmosphere"
                break;
            default :
                type = "Unknown"
                break;
            }
        }
        return type
    }
}
