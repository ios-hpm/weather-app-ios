//
//  Alert.swift
//  ClimaApp
//
//  Created by HPM on 05/08/2019.
//  Copyright © 2019 HPM. All rights reserved.
//

import Foundation
import UIKit

class Alert {
    
    static func displayAlert(title: String? = "Alert", message: String, viewController: UIViewController) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        viewController.present(alert, animated: true)
    }
    
}
