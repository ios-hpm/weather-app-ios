//
//  CurrentLocationViewControllerExtension.swift
//  ClimaApp
//
//  Created by HPM on 05/08/2019.
//  Copyright © 2019 HPM. All rights reserved.
//

import Foundation
import CoreLocation

extension CurrentLocationViewController: CLLocationManagerDelegate{
    
    // After user tap on 'Allow' or 'Disallow' on the dialog
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == .authorizedWhenInUse || status == .authorizedAlways){
            manager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // .requestLocation will only pass one location to the locations array
        // hence we can access it by taking the first element of the array
        if let location = locations.first {
            
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            
            let distanceInMeters = location.distance(from: CLLocation(latitude: self.latitude, longitude: self.longitude))
            
            if distanceInMeters > 5000 {
                getWeatherByCurrentLocation(with: latitude, and: longitude)
                updateCoordinates(latitude,longitude)
                updateDate()
            }
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // might be that user didn't enable location service on the device
        // or there might be no GPS signal inside a building
        displayAlert(message:"You are in a place without GPS signal")
    }
    
    func addDelegateTo(_ locationManager: CLLocationManager){
        locationManager.delegate = self
    }
    
}
