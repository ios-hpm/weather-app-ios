//
//  CustomLocationViewController.swift
//  ClimaApp
//
//  Created by HPM on 02/08/2019.
//  Copyright © 2019 HPM. All rights reserved.
//

import UIKit
import os.log

class CustomLocationViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: properties
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    var searchedLocation : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        updateSaveButtonState()
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        // Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIButton, button === searchButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        
        searchedLocation = searchTextField.text ?? ""
     
    }
    
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchButton.isEnabled = false
    }
    
    //MARK: private methods
    private func updateSaveButtonState() {
        let text = searchTextField.text ?? ""
        searchButton.isEnabled = !text.isEmpty
    }
    
    //MARK: actions
    @IBAction func searchButtonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
    }
}
