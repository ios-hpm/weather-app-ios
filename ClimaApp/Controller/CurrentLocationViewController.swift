//
//  ViewController.swift
//  ClimaApp
//
//  Created by HPM on 01/08/2019.
//  Copyright © 2019 HPM. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

class CurrentLocationViewController: UIViewController {
    
    //MARK: properties
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var anotherDataLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherTypeLabel: UILabel!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    let locationManager = CLLocationManager()
    var latitude: CLLocationDegrees = 0.0
    var longitude: CLLocationDegrees = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addDelegateTo(locationManager)
        locationManager.requestWhenInUseAuthorization()
        retriveCurrentLocation()
        
    }
    
    //MARK: private methods
    private func retriveCurrentLocation() {
        
        let status = CLLocationManager.authorizationStatus()
        
        if status == .denied || status == .restricted || !CLLocationManager.locationServicesEnabled() {
            
            displayAlert(message:"You need to allow location for retrieving temp")
            return
        }
        
        // if haven't show location permission dialog before, show it to user
        if status == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
            return
        }
        
        // start monitoring location data and get notified whenever there is change in location data / every few seconds, until stopUpdatingLocation() is called
        locationManager.startUpdatingLocation()
    }
    
    func updateDate() {
        
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        
        let year =  components.year!
        let day = components.day!
        
        let dayOfWeek = date.dayOfWeek()!.prefix(3)
        let monthOfYear = date.monthOfYear()!.prefix(3)
        
        let result = "\(dayOfWeek) \(monthOfYear) \(day), \(year)"
        dateLabel.text = result
        
        let hh2 = (Calendar.current.component(.hour, from: Date()))
        let mm2 = (Calendar.current.component(.minute, from: Date()))
        
        if mm2 < 10 {
            timeLabel.text = "\(hh2):0\(mm2)"
            
        } else {
            timeLabel.text = "\(hh2):\(mm2)"
            
        }
        
    }
    
    func getWeatherByCurrentLocation(with latitude: Double, and longitude: Double) {
        
        let paramaters: Parameters = [
            "lat": latitude,
            "lon": longitude,
            "appid": "e72ca729af228beabd5d20e3b7749713"
        ]
        
        let url = "https://api.openweathermap.org/data/2.5/weather"
        
        Alamofire.request(url, method: .get, parameters: paramaters, encoding: URLEncoding.default)
            .responseJSON { (response) in
                
                switch response.result {
                case .success(let value) :
                    
                    guard let dict = value as? [String:Any] else { return }
                    self.unpackData(dict:dict)
                    break
                    
                case .failure(let error) :
                    print(error.localizedDescription)
                    break
                }
        }
    }
    
    func getWeatherBy(cityName: String) {
        
        let paramaters: Parameters = [
            "q": cityName,
            "appid": "e72ca729af228beabd5d20e3b7749713"
        ]
        
        let url = "https://api.openweathermap.org/data/2.5/weather"
        
        Alamofire.request(url, method: .get, parameters: paramaters, encoding: URLEncoding.default)
            .responseJSON { response in
                
                let myResponse = response.result
                
                switch myResponse {
                    
                case .success(let value) :
                    
                    guard let dict = value as? [String:Any] else { return }
                    self.unpackData(dict: dict)
                    break
                    
                case .failure(let error) :
                    print(error.localizedDescription)
                    break
                }
                
                
        }
    }
    
    func unpackData(dict: [String:Any]) {
        
        guard let code = dict["cod"] as? Int else {
            locationLabel.text = "Unknown location"
            displayAlert(message:"Unknown location")
            return
        }
        
        guard let main = dict["main"] as? [String:Any] else {return}
        print(main)
        guard let max = main["temp_max"] as? Double else {return}
        guard let min = main["temp_min"] as? Double else {return}
        guard let hum = main["humidity"] as? Double else {return}
        guard let temp = main["temp"] as? Double else {return}
        
        guard let cloudsData = dict["clouds"] as? [String:Any] else {return}
        print(cloudsData)
        guard let clouds = cloudsData["all"] as? Double else {return}
        
        guard let windData = dict["wind"] as? [String:Any] else {return}
        print(windData)
        guard let wind = windData["speed"] as? Double else {return}
        
        guard let location = dict["name"] as? String else {return}
        
        guard let iconData = dict["weather"] as? [Any] else {return}
        guard let iconData1 = iconData[0] as? [String:Any] else {return}
        guard let iconCode = iconData1["icon"] as? String else {return}
        guard let id = iconData1["id"] as? Int else {return}
        
        let weather = WeatherData(temp: temp,
                                  humidity: hum,
                                  max: max,
                                  min: min,
                                  windSpeed: wind,
                                  clouds: clouds,
                                  location: location,
                                  code: "\(code)",
                                  id: id)
        
        self.updateWeatherInfo(weather: weather)
        self.updateWeatherIcon(iconCode: iconCode)
    }
    
    func updateWeatherInfo(weather: WeatherData) {
        
        locationLabel.text = weather.location
        
        let type = weather.determineType()
        let anotherData = """
        Humidity:\(weather.humidity)
        Max:\(weather.max)
        Min:\(weather.min)
        Wind speed:\(weather.windSpeed)
        Clouds:\(weather.clouds)
        """
        
        anotherDataLabel.text = anotherData
        tempLabel.text = String(weather.temp) + "°C"
        weatherTypeLabel.text = type
        
    }
    
    func updateWeatherIcon(iconCode: String) {
        
        let imageUrlString = "https://openweathermap.org/img/wn/\(iconCode)@2x.png"
        guard let url = URL(string: imageUrlString) else { return }
        ViewManager().downloadImage(from: url, imageView: imageView)
    }
    
    func displayAlert(message: String) {
        Alert.displayAlert(message: message, viewController: self)
    }
    
    func updateCoordinates(_ latitude: CLLocationDegrees, _ longitude: CLLocationDegrees) {
        
        self.latitude = latitude
        self.longitude = longitude
    }
    
    //MARK: actions
    @IBAction func reloadTapped(_ sender: Any) {
        getWeatherByCurrentLocation(with: latitude, and: longitude)
    }
    
    @IBAction func unwindToVC1(sender: UIStoryboardSegue) {
        
        if let sourceViewController = sender.source as? CustomLocationViewController {
            let searchedLocation = sourceViewController.searchedLocation
            getWeatherBy(cityName: searchedLocation)
        }
        
    }
    
    @IBAction func cancelUnwind(sender: UIStoryboardSegue) {
        
    }
    
    @IBAction func cancel7daysUnwind(sender: UIStoryboardSegue) {
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "7daysSegue"{
            var DestViewController = segue.destination as! UINavigationController
            let targetController = DestViewController.topViewController as! DailyWeatherTableViewController
            targetController.latitude = self.latitude
            targetController.longitude = self.longitude
            targetController.location = locationLabel.text ?? ""

        }
        
    }

}


