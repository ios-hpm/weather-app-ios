//
//  DailyWeatherTableViewController.swift
//  ClimaApp
//
//  Created by HPM on 05/08/2019.
//  Copyright © 2019 HPM. All rights reserved.
//

import UIKit
import Alamofire

struct DayWeather {
    let dayName: String
    let temp: Double
}

class DailyWeatherTableViewController: UITableViewController {
    
    //MARK: properties
    @IBOutlet weak var locationLabel: UILabel!
    
    var data = [[String:Double]]()
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var location: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getWeather()
        locationLabel.text = location
        self.tableView.tableFooterView = UIView()

    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =
            tableView.dequeueReusableCell(withIdentifier:
                "weatherCell") else {
                    fatalError("Could not dequeue a cell")
        }
        
        let myData = data[indexPath.row]
        for (key, value) in myData {
//            cell.textLabel?.text = "\(key) : \(value)°C"
            cell.textLabel?.text = key
            cell.detailTextLabel?.text = "\(value)°C"
        }
        return cell
        
    }
    
    //MARK: private methods
    func getWeather() {
        print("ceva")
        let paramaters: Parameters = [
            "q": location,
            "cnt": 7,
            "appid": "e72ca729af228beabd5d20e3b7749713"
        ]
        
        let url = "https://api.openweathermap.org/data/2.5/forecast/daily?"
        
        Alamofire.request(url, method: .get, parameters: paramaters, encoding: URLEncoding.default)
            .responseJSON { (response) in
                
                switch response.result {
                case .success(let value) :
                    
                    var tempList = [Double]()
                    guard let dict = value as? [String:Any] else { return }
                    guard let list = dict["list"] as? [Any] else {return}
                    
                    for some in list{
                        
                        guard let val = some as? [String:Any] else {return}
                        guard let temp = val["temp"] as? [String:Any] else {return}
                        guard let temp1 = temp["day"] as? Double else {return}
                        
                        let temp2 = floor(1000*(temp1-Double(273.15))/1000)
                        tempList.append(temp2)
                        print(temp2)
                        
                    }
                    
                    self.populateTable(with: tempList)
                    
                    break
                    
                case .failure(let error) :
                    print(error.localizedDescription)
                    break
                }
        }
    }
    
    private func populateTable(with tempList: [Double]) {
        
        let startDate = Date()
        self.data.append(["Today":tempList[0]])
        for i in 1...6 {
            let nextDate = Calendar.current.date(byAdding: .day, value: i, to: startDate)
            self.data.append([nextDate?.dayOfWeek() ?? "":tempList[i]])
        }
        
        self.tableView.reloadData()
    }
    
}
